package lab1;

import cse131.ArgsProcessor;

public class Nutrition {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
ArgsProcessor ap = new ArgsProcessor(args);
		
		String name = ap.nextString("The name of this food?");
		double carbs = ap.nextDouble("The number of grams of carbohydrates in this food");
		double carbCals = carbs * 4;
		double fat = ap.nextDouble("The number of grams of fat in this food");
		double fatCals = fat * 9;
		
		double protein = ap.nextDouble("The number of grams of protein in this food");
		double proteinCals = protein * 4;
		double statedCals = ap.nextDouble("The number of calories stated on this food�s label");
		double fiber = carbCals + fatCals + proteinCals - statedCals;
		double fiberGrams = fiber / 4;
		double p1 = Math.round(carbCals / statedCals * 1000) / 10.0;
		
		double p2 = Math.round(fatCals / statedCals * 1000)/10.0;
	
		
		double p3 = Math.round(proteinCals / statedCals * 1000)/10.0;
		System.out.println(name + " has " + carbs + " grams of carbohydrates = " + carbCals + " Calories, " + fat + " grams of fat = " + fatCals + " Calories, " + protein + " grams of protein = " + proteinCals + " Calories.");
        System.out.println("This food is said to have " + statedCals + " (available) calories. With " + fiber + " unavailable Calories, this food has " + fiberGrams + " grams of fiber");
        System.out.println("Approximately");
        System.out.println(p1 + "% of your food is carbohydrates");
        System.out.println(p2 + "% of your food is fat");
        System.out.println(p3 + "% of you food is protein");
        boolean isLowCab = p1 <= 0.25;
        System.out.println("This food is acceptable for a low-carb diet? " + isLowCab);
        boolean isLowFat = p2<= 0.15;
        System.out.println("This food is acceptable for a low-fat diet? " + isLowFat);
        boolean flip = Math.random() > 0.5;
        System.out.println("By coin flip, you should eat this food? " + flip);
	}

}

package lab2;

import cse131.ArgsProcessor;

public class RPS {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
ArgsProcessor ap = new ArgsProcessor(args);
int numRounds = ap.nextInt("how many rounds of RPS to play?");
String rotateChioce = ("ro");
double t2 = Math.random();
int winTimes1 = 0;
int winTimes2 = 0;
double i = 1.0;

for(i=1; i<=numRounds; i=i+1) {
	System.out.println(i);
	double t = Math.random();
	
	String randomChioce;
	
	// assign move to random player
	if(t< 1/3.0) {
		randomChioce = ("rock");
	}		
	else if(t< 2/3.0) {
		randomChioce = ("paper");
	}
	else {
		randomChioce = ("scissors");
	}
	System.out.println("randomChoice is " + randomChioce);
	
	// assign move to rotating player
	if(i%3 ==0) {
		rotateChioce = ("rock");
	}
	else if(i%3 == 1) {
		rotateChioce = ("paper");	
	}
	else if(i%3 ==2) {
		rotateChioce = ("scissors");
	}
	
	System.out.println("rotateChoice is " + rotateChioce);
	
	if(randomChioce == rotateChioce) {
		System.out.println("The result is a tie.");
	}
	else if(randomChioce == "rock") {
		if(rotateChioce == "scissors") {
			System.out.println("The first player wins.");
			winTimes1 = winTimes1 +1;
		}
		else {
			System.out.println("The second player wins.");
			winTimes2 = winTimes2 + 1;
		}
	}
	else if(randomChioce == "paper") {
		if(rotateChioce == "rock") {
			System.out.println("The first player wins");
			winTimes1 = winTimes1 +1;
		}
		else {
			System.out.println("The second player wins.");
			winTimes2 = winTimes2 + 1;
		}
	}
	else {
		if(rotateChioce == "paper") {
			System.out.println("The first player wins");
			winTimes1 = winTimes1 +1;
		}
		else {
			System.out.println("The second player wins.");
			winTimes2 = winTimes2 + 1;
		}
	}
}
    String time1 = ("times");
    String time2 = ("times");
    if(winTimes1 == 1.0) {
    	time1 = "time";
    }
    if(winTimes2 == 1.0) {
    	time2 = "time";
    }

    System.out.println("The first player wins " + winTimes1 + " " + time1);
	System.out.println("The first player wins " + Math.round(winTimes1/(i-1)*1000)/10.0 + "% of games.");
	System.out.println("The second player wins " + winTimes2 + " " + time2);
	System.out.println("The second player wins " + Math.round(winTimes2/(i-1)*1000)/10.0 + "% of games.");
	}

}

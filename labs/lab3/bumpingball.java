package lab3;

import cse131.ArgsProcessor;
import sedgewick.StdDraw;

public class bumpingball {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		ArgsProcessor ap = new ArgsProcessor(args);
		int B = ap.nextInt("How many balls?");
		double[] xpos = new double[B-1];
			double[] ypos = new double[B-1];
			double[] xVel = new double[B-1];
			double[] yVel = new double[B-1];
			int i;
		for(i=0; i<B; i++) {
			StdDraw.setScale(-1, 1);
			xpos[i] = Math.random();
			ypos[i] = Math.random();

			xVel[i] = Math.random();
			yVel[i] = Math.random();
			if((xVel[i]<0.01) | (xVel[i]>0.03)) {
					xVel[i] = Math.random();
				}
				else if((yVel[i]<0.01) | (yVel[i]>0.03)) {
					yVel[i] = Math.random();
				}
		}
			while(true) {
				
				StdDraw.clear();
				StdDraw.filledCircle(xpos[i], ypos[i], .05);
				//set new position
				xpos[i] += xVel[i];
				ypos[i] += yVel[i];
				//check if it hits a wall...
				//..if it does, reverse
				if(Math.abs(ypos[i]) >= 1) {
					yVel[i] = -yVel[i];
				}
				if(Math.abs(xpos[i]) >= 1) {
					xVel[i] = -xVel[i];
				}
				StdDraw.show(1000);
			}
		}

	}



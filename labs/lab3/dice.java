package lab3;

import cse131.ArgsProcessor;

public class dice {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		ArgsProcessor ap = new ArgsProcessor(args);

		int D = ap.nextInt("The number of dice used in this simulation");
		int T = ap.nextInt("The number of times the dice are thrown");
		int[] sum = new int[T];
		int[] diceThrown = new int[D];
		int sameValue = 0;
		int[] sameSum = new int[D*6];
		for(int i=0; i<T; i++) {
			System.out.print(i+1 + ": ");
			for(int j=0; j<D; j++) {
				//for each die d
				//     randomly throw d to obtain the value v, shown face-up on d
				// end  
				int diceValue = (int)(Math.random() * 6) + 1;

				// for each die d
				//     randomly throw d to obtain the value v, shown face-up on d
				//     record die d's role in the diceThrown array
				// end
				// At this point, your dieThrown array has an entry that shows each die's value
				diceThrown [j] = diceValue;
				System.out.print(diceThrown[j] + ", ");
				//



				//compute the sum
				sum[i] = sum[i] + diceThrown[j];
				
			}
			for(int k=0; k<D*6; k++) {
				if(sum[i] == k) {
					sameSum[k] = sameSum[k]+1;
				}
			}
			
			System.out.println();
			int j=0;

			while((j<D) & (diceThrown[j] == diceThrown[0])) {

				j++;
			}
			if(j==D) {
				sameValue = sameValue ++;
			}
		}



		for(int i=0; i<T; i++) {
			System.out.println("the sum at " + (i+1) + " is: " + sum[i]);
		}
		if(sameValue>0) {
			if(sameValue>1) {
				System.out.println(" all of the dice show the same value " + sameValue + " times.");
			}
			else  {
				System.out.println(" all of the dice show the same value 1 time.");
			}
			System.out.println("the fraction of times they all have the same value is " + (sameValue/T*100) + "%.");
		}
		for(int k=0; k<D*6; k++) {
			if(sameSum[k] > 1) {
				System.out.println("sum " + k + " shows " + sameSum[k] + "times.");
			}
			else if(sameSum[k] > 0) {
				System.out.println("sum " + k + " shows 1 time.");
			}
		}
	}

}

package exercises3;

import java.awt.Color;
//import java.awt.color.*;

import sedgewick.StdDraw;

public class Smiley {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		//draw a smiling face
		Color [] smileyColors = {Color.blue, Color.GREEN, Color.blue, Color.pink, Color.red};
		int i=0;
		while(true) {
			StdDraw.clear();

			
		
		
		StdDraw.setPenColor(smileyColors[i%4]);
		StdDraw.filledCircle(.5, .5, .4);
		StdDraw.setPenRadius(.05);
		StdDraw.setPenColor(smileyColors[(i+1)%4]);
		StdDraw.point(.3, .6);
		StdDraw.point(.7, .6);
		StdDraw.arc(.5, .4, .2, 180, 0);

		while(!StdDraw.mousePressed()) {
			StdDraw.pause(10);

		}
		
		while(StdDraw.mousePressed()) {
			StdDraw.pause(10);
		}

		StdDraw.show(10);
		i++;
		}
	}

}


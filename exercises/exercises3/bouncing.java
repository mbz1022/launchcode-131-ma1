package exercises3;

import sedgewick.StdDraw;

public class bouncing {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		StdDraw.setScale(-1, 1);
		double xpos=0;
		double ypos=0;
		double xVel=.03;
		double yVel=.02;
		
		while(true) {
			StdDraw.clear();
			StdDraw.filledCircle(xpos, ypos, .05);
			//set new position
			xpos += xVel;
			ypos += yVel;
			//check if it hits a wall...
			//..if it does, reverse
			if(Math.abs(ypos) >= 1) {
				yVel = -yVel;
			}
			if(Math.abs(xpos) >= 1) {
				xVel = -xVel;
			}
			StdDraw.show(100);
		}
		//StdDraw.filledCircle(-.5, -.5, .2);
	}

}

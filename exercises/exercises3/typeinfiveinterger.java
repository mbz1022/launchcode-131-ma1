package exercises3;

import cse131.ArgsProcessor;

public class typeinfiveinterger {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		ArgsProcessor ap = new ArgsProcessor(args);
		int [] array = new int[5];
		for(int i=0; i<5; ++i) {
			//int answer = ap.nextInt("value" + i);
			//array[i] = answer;
			array[i] = ap.nextInt("type in an integer for "+ i);
			System.out.println(array[i] + " ");

		}
		int max = 0;
		for(int i=0; i<array.length; ++i) {
			if(array[i] > max) {
				max = array[i];
			}

		}
		System.out.println("max number is " + max);
		//find the min
		int min = 100;
		for(int i=0; i<array.length; ++i) {
			if(array[i]<min) {
				min = array[i];
			}    
		}
		System.out.println("min number is " + min);

		//find average
		int total =0;
		int avg;
		for(int i=0; i<array.length; ++i) {
			total += array[i];
		}
		avg = total / array.length;
		System.out.println("Avg value is " + avg);


		//create 3*3 array with random numbers

		int [][] array2 =new int [3][3];
		for(int i=0; i<array2.length; ++i) {
			for(int j=0; j<array2.length; ++j) {
				int rand = (int)(Math.random() * 10);
				array2[i][j] = rand;
				System.out.print(array2[i][j]);
				if(j<array2.length) {
					System.out.print(" ");
				}
				else {System.out.println();
				}
			}
			//System.out.println();
		}

	}


	//sum of columns
	int sum = 0;
	int [] colSum = new int[array2.length];
	for(int i=0; i<array2.length; ++i) {
		for(int j=0; j<array2.length; ++j) {
			sum += array2[i][j];
			colSum[j] += array2[i][j];
		}
	}
	for(int i=0; i<array2.length; ++i) {
		System.out.println("sum of column " + i + ":" + colSum[i]);
	}
}
}

package exercises2;

import cse131.ArgsProcessor;

public class Heads {

	public static void main(String[] args) {

		// what are the concepts?
		ArgsProcessor ap = new ArgsProcessor(args);
		int N = ap.nextInt("Enter N:");
		int numHeads = 0;
		int numFlips = 0;
		int sumFlips = 0;
		
		for(int i=0; i<N; i=i+1) {
			System.out.println(i);

		// now flip a coin until we see 10 heads
		    while (numHeads != 10) {
			     boolean isHeads = Math.random() < 0.5;
			     if (isHeads) {
				     numHeads = numHeads + 1;
			    }
			     numFlips = numFlips + 1;

		    }
		// here, numHeads should be 10
		
		    System.out.println("Number of flips was " + numFlips);
		    sumFlips = sumFlips + numFlips;
		
		

	}
		System.out.println("sum=" + sumFlips);
		System.out.println("The average number of flip coin and get 10 heads is: " + (1.0*sumFlips / N));

	}	

}

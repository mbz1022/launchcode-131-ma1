package exercises2;

import cse131.ArgsProcessor;

public class grid {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		ArgsProcessor ap = new ArgsProcessor(args);
		int rows = ap.nextInt("how many rows you like");
		int columns = ap.nextInt("how many columns you like");
		for(int j=0; j<rows; ++j) {
			System.out.println();
			for(int i=0; i<columns; ++i) {
				System.out.print("* ");
			}
		}
	}

}

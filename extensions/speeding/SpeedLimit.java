package speeding;

import cse131.ArgsProcessor;

public class SpeedLimit {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		ArgsProcessor ap = new ArgsProcessor(args);
		int speed = ap.nextInt("How many MPH were you dring?");
		int speedLimit = ap.nextInt("What is the speed limit?");
		int difference = speed - speedLimit;
		int fine = (difference > 9) ? 50 + (difference - 10) * 10 : 0;
		String speedString = (difference >9) ? "You were speeding and must pay $" + fine : "You were not speeding.";
		System.out.println(speedString);
		
	}

}

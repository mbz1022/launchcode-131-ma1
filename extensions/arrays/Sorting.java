package arrays;
import cse131.ArgsProcessor;

public class Sorting {

	public static void main(String[] args) {
		ArgsProcessor ap = new ArgsProcessor(args);
		int size = ap.nextInt("enter the size of the collection");
		if(size<0) {
			size = ap.nextInt("this is a negative number, please enter the size of the collection");
		}
		int[] numbers = new int[size];
		for(int i=0; i<size; i++) {
			numbers[i] = ap.nextInt("enter a number:");

		}
		for(int i=0; i<size; i++) {
			System.out.print(numbers[i] + " ");
		}
		int sortCount = 0;
		int j;
		while(sortCount < size) {
			for(int i=0; i<size; i++) {
				int min = numbers[i];
				for(j=1; j<size; j++) {
					if(min > numbers[j]) {
						min = numbers[j];

					}					
				}
				if(numbers[i] != min) {
					numbers[j] = numbers[i];
					numbers[i] = min;
				}
			}

			sortCount++;
		}
		for(int i=0; i<size; i++) {
			System.out.println("sorted numbers : " + numbers[i]);
		}
	}

}

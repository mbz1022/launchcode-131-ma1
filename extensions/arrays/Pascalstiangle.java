package arrays;

import cse131.ArgsProcessor;

public class Pascalstiangle {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		ArgsProcessor ap = new ArgsProcessor(args);
		int n = ap.nextInt("how many rows and columns");
		int[][] numbers = new int[n][n];
		for(int i=0; i<n; i++) {
			for(int s=1; s<n-i; s++) {
					System.out.print("  ");
				}
			for(int j=0; j<n; j++) {
				if(j == 0) {
					numbers[i][j] = 1;
				}
				else if(i == j) {
					numbers[i][j] = 1;
				}
				else if(j>i) {
					numbers[i][j] = 0;
				}
				else {
					numbers[i][j] = numbers[i-1][j] + numbers[i-1][j-1];
				}
				
				if(numbers[i][j] != 0) {
					System.out.print(numbers[i][j] + "   ");
				}
			}
			System.out.println();
		}

	}

}

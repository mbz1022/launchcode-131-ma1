package arrays;
import cse131.ArgsProcessor;

public class PascalsTriangle {
	
	public static void main(String[] args) {
	ArgsProcessor ap = new ArgsProcessor(args);
	int N = ap.nextInt("the number of rows");
	int C = ap.nextInt("the number of columns");
	int[][] numbers = new int[N][C];
	System.out.println("     column");
	System.out.print("     ");
	for(int i=0; i<N; i++) {
	System.out.print(i + " ");
	}
	System.out.println();
	System.out.println("row");
	for(int i=0; i<N; i++) {
		System.out.print(i + "    ");
		for(int j=0; j<C; j++) {
			if(j==0) {
			numbers[i][j] = 1;
			}
			else if(i==j) {
				numbers[i][j] = 1;
			}
			else if(j > i) {
				numbers[i][j] = 0;
			}
			else {
				numbers[i][j] = numbers[i-1][j] + numbers[i-1][j-1];
			}
			if(numbers[i][j] != 0) {
				System.out.print(numbers[i][j] + " ");
			}
		}
		System.out.println();
	}
	}
}

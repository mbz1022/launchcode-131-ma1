package studio2;

import cse131.ArgsProcessor;

public class Gambler {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		ArgsProcessor ap = new ArgsProcessor(args);
		int startAmount = ap.nextInt("Enter startAmount:");
		double winChance = ap.nextDouble("Enter winChance:");
		int winAmount = ap.nextInt("Enter winAmount:");
		int totalPlays = ap.nextInt("Enter totalPlays:");
		double lossChance = 1-winChance;
		int currentMoney = startAmount;
		int rounds=0;
		String winLose;
		int losses = 0;
		
		
		//all simulations
		for(int i=1; i<=totalPlays; ++i){
			rounds = 0;
			currentMoney = startAmount;
			//all rounds/one simulation
			while((currentMoney > 0) && (currentMoney < winAmount)) {
				double gamble = Math.random();
				if(gamble < winChance){
					currentMoney++; //= currentMoney + 1;
				}
				else {
					currentMoney--; //= currentMoney - 1;
				}
				rounds=rounds+1;	
				

			}
			System.out.println(rounds);
			
			if(currentMoney == winAmount) {
				winLose = "win";	
			}
			else {
				winLose = "lose";
				losses = losses+1;
			}
			System.out.println("Simulation " + i + ": " + rounds + " rounds    " + winLose);
		}
		
        System.out.println("Loses: " + losses + "   simulations: " + totalPlays);
        double Ruin;
        if (lossChance != winChance) {
        	Ruin =  (Math.pow((lossChance/winChance),startAmount) - Math.pow((lossChance/winChance),winAmount)) / (1 - Math.pow((lossChance/winChance),winAmount)); 

        }
        else {
        	Ruin = 1 - startAmount / winAmount;
        }
       
        System.out.println("Actual Ruin Rate: " + Ruin);
	}
	

}

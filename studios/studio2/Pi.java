
		// TODO Auto-generated method stub
		package studio2;
		public class Pi {

		  public static void computePi() {
		     double ans = 0.0;
		     int darts = 1000;
		     int in = 0;
		     //area = pi*r^2
		     for(int i =0; i<darts; i++) {
		    	 double x = Math.random();
		    	 double y = Math.random();
		    	 double xdist = Math.pow((x-0.5),2);
		    	 double ydist = Math.pow((y-0.5),2);
		    	 double distance = Math.sqrt(xdist+ ydist);
		    	 
		    	 if(distance < 0.5) {
		    		 in++;
		    	 }
		    	 /*
		    	  * darts in the circle/total darts
		    	  * random x, random y
		    	  * double distance = sqrt((x-0.5)^2+ (y-0.5)^2)
		    	  * if(distance < 0.5)
		    	  *     count the darts in the circle
		    	  *     in++
		    	  */
		     }
		     
		     ans =  ((double)in/darts) /Math.pow(0.5,2);

		     //  fill in to compute ans = Pi

		     System.out.println("Our group shows Pi = " + ans);
		  }

		  public static void main(String[] args) {
			  /*
			   * NOTE 
			   */
		     computePi();
		  }
		}




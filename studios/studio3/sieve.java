package studio3;

import cse131.ArgsProcessor;

public class sieve {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		ArgsProcessor ap = new ArgsProcessor(args);
		int n = ap.nextInt("size of the array");
		int[] sieve = new int[n];
		//populating array value 2 to n+1
		for(int r=0; r<n; ++r) {
			sieve[r] = r+2;
			System.out.print(sieve[r] + " ");

		}

		//iterating over array, checking for anything that's a multiple of a lower number
		//if it is, assign it zero
		
		
		
		for(int factor=2; factor<sieve.length/2; factor++) { 
			//only need to run up to sieve.length/2,nothing will be a multiple of something higher than that

			if(sieve[factor-2] != 0) {//if wieve
				for(int r=factor-1; r<sieve.length; ++r) {
					if(sieve[r]%factor == 0) {
						sieve[r] = 0;
					}
				}
			}
		}
		//creating a new primes array
		int[] primes = new int[sieve.length];
		int index = 0;

		System.out.println();
		for(int i=0; i<sieve.length; i++) {
			if(sieve[i] != 0) {
				primes[index] = sieve[i];
				index++;
				
				System.out.print(sieve[i] + " ");
			}

		}
		System.out.println(" ");
		for(int i=0; i<primes.length; i++) {
			System.out.print(" ");
			System.out.print(primes[i]);
		}
		System.out.print(primes[3]);
	}
}